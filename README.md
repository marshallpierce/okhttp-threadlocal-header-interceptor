 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/okhttp-threadlocal-header-interceptor/images/download.svg) ](https://bintray.com/marshallpierce/maven/okhttp-threadlocal-header-interceptor/_latestVersion) 

# Deprecated

This has been superseded by [http-client-threadlocal-header](https://bitbucket.org/marshallpierce/http-client-threadlocal-header/src/master/).

## What is it?

An [OkHttp](http://square.github.io/okhttp/) `Interceptor` that uses data from a `ThreadLocal` to set headers on outbound requests.

## Why is that useful?

Sometimes you have context data (a request id, or distributed tracing data, or similar) that you would like to propagate to calls to other services via headers. This makes it simpler to do so: set the data in a `ThreadLocal` and then any calls made from that thread will have the desired header. 

Plain `ThreadLocal`s are better than nothing, but they're inconvenient when using threadpools. Using Kotlin coroutines with [`asContextElement`](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/java.lang.-thread-local/as-context-element.html) makes this much more useful in practice.

## Usage

```kotlin
val threadLocal = ThreadLocal.withInitial<String?> { null }
val interceptor = ThreadLocalHeaderInterceptor("x-your-favorite-header", threadLocal)
val client = OkHttpClient.Builder()
        .addNetworkInterceptor(interceptor)
        .build()
```
