package org.mpierce.okhttp.threadlocal

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.jackson.JacksonConverter
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import okhttp3.OkHttpClient
import okhttp3.Request
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

internal class ThreadLocalHeaderInterceptorTest {
    private lateinit var server: ApplicationEngine

    private val port = 12000

    private val headerName = "x-call-id"

    private val defaultHeaders = mapOf(
            "Host" to listOf("localhost:12000"),
            "Connection" to listOf("Keep-Alive"),
            "Accept-Encoding" to listOf("gzip"),
            "User-Agent" to listOf("okhttp/4.0.0")
    )

    @BeforeEach
    internal fun setUp() {
        server = embeddedServer(Netty, port = port) {
            install(ContentNegotiation) {
                register(ContentType.Application.Json,
                        JacksonConverter(mapper))
            }
            install(Routing) {
                get("/headers") {
                    call.respond(call.request.headers
                            .entries()
                            .map { Pair(it.key, it.value) }
                            .toMap())
                }
            }
        }
        server.start(wait = false)
    }

    @AfterEach
    internal fun tearDown() {
        server.stop(10, 10, TimeUnit.MILLISECONDS)
    }

    @Test
    internal fun noHeaderWhenNoContext() {
        val tl = ThreadLocal.withInitial<String?> { null }
        val interceptor = ThreadLocalHeaderInterceptor(headerName, tl)
        val client = OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                .build()

        val reqHeaders = callHeadersEndpoint(client)
        assertEquals(defaultHeaders, reqHeaders)
    }

    @Test
    internal fun sendsHeaderWhenContextPresent() {
        val tl = ThreadLocal.withInitial<String?> { null }
        val interceptor = ThreadLocalHeaderInterceptor(headerName, tl)
        val client = OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                .build()

        tl.set("foo")
        val reqHeaders = callHeadersEndpoint(client)
        assertEquals(defaultHeaders
                .plus(headerName to listOf("foo")),
                reqHeaders)
    }

    private fun callHeadersEndpoint(client: OkHttpClient): Map<String, List<String>> {
        val resp = client.newCall(Request.Builder().url("http://localhost:$port/headers").build()).execute()
        return resp.body.use {
            mapper.reader()
                    .forType(object : TypeReference<Map<String, List<String>>>() {})
                    .readValue(it!!.byteStream())
        }
    }
}

private val mapper = ObjectMapper().apply {
    registerModule(KotlinModule())
}
