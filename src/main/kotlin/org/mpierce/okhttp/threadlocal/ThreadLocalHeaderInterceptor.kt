package org.mpierce.okhttp.threadlocal

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Sets the specified header to the content of the thread local, if the content is non-null.
 *
 * In other words, if the thread local has a non-null value, a header will be added with that value, and if it's null,
 * no header will be added.
 */
class ThreadLocalHeaderInterceptor(private val headerName: String,
                                   private val threadLocal: ThreadLocal<String?>) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val origReq = chain.request()

        val contents = threadLocal.get()
        val req = if (contents != null) {
            origReq.newBuilder()
                    .addHeader(headerName, contents)
                    .build()
        } else {
            origReq
        }

        return chain.proceed(req)
    }
}
